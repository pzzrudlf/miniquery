/**
*yet another JavaScript Library that inspired by jQuery
*@author pzzrudlf <pzzrudlf@gmail.com>
*@description supports CommonJS, AMD. Do not use in production!!!
*/
;(function(window, undefined){

	//定义内部变量并将miniQuery和$暴露到window全局对象下
	var miniQuery = window.miniQuery = window.$ = function(selector){
		return new miniQuery.fn.init(selector);
	};

	//处理原型对象
	miniQuery.fn = miniQuery.prototype = {
		init:function(selector){

			//tagName
			var eles = document.getElementsByTagName(selector);

			Array.prototype.push.apply(this, eles);

			return this;
		},
		miniQuery:"1.0.0",
		length:0,
		size:function(){
			return this.length;
		}

	};
	miniQuery.fn.init.prototype = miniQuery.fn;

	//实现继承,并且只处理只有一个参数(插件的扩展),简单循环复制，后期更新继承方法,
	miniQuery.extend = miniQuery.fn.extend = function(){
		var o = arguments[0];
		for(var p in o){
			// if () {
				this[p] = o[p];
			// }
		}
	};

	//添加静态方法
	miniQuery.extend({
		trim:function(text){
			return (text||"").replace(/^\s+|\s+$/g,"");
		}
	});
	
	//添加实例方法
	miniQuery.fn.extend({
		get:function(index){
			return this[index];
		},
		each:function(fn){
			for(var i =0;i<this.length;i++){
				fn(i,this[i]);
			}
			return this;
		},
		css:function(){
			var l = arguments.length;
			if (l == 1){
				return this[0].style[arguments[0]];
			} else {
				var name = arguments[0];
				var value = arguments[1];
				this.each(function(i,e){
					e.style[name] = value;
				});
			}
			return this;
		}
	});
	
	//CommonJS || AMD || window.miniQuery
	if ( typeof module === "object" && module && typeof module.exports === "object" ) {
		module.exports = miniQuery;
	} else if ( typeof define === "function" && define.amd ) {
		define("miniquery", [], function () {
			return miniQuery;
		});
	}

	if ( typeof window === "object" && typeof window.document === "object" ) {
		window.miniQuery = window.$ = miniQuery;
	}

})(window);